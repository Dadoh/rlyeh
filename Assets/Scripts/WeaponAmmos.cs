﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponAmmos : MonoBehaviour {

    public int totalFireAmmo = 30;
    public int totalBombAmmo = 25;
    public int totalStoneAmmo = 15;
    public int totalSwordAmmo = 45;
    public int totalWhipAmmo = 10;
    public int totalProjectileAmmo = 50;

    public int currentFireAmmo;
    public int currentBombAmmo;
    public int currentStoneAmmo;
    public int currentSwordAmmo;
    public int currentWhipAmmo;
    public int currentProjectileAmmo;

	// Update is called once per frame
	void Update () {
       
	}
}
